<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mountain */

$this->title = Yii::t('app', 'Create Mountain');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mountains'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mountain-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
